namespace GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using GameLogic;
    using GameModel;
    using GameRenderer;
    using GameRepository;

    /// <summary>
    /// Class that implements the games control component.
    /// </summary>
    public class Control : FrameworkElement
    {
        private IGameModel model;
        private IStorageRepository storageRepository;
        private IGameLogic logic;
        private DungeonRenderer renderer;
        private IResource resource;
        private Stopwatch sw;

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Gets or sets the loading type.
        /// </summary>
        public StartGame Loadtype { get; set; }

        /// <summary>
        /// Gets or sets the path to load the game.
        /// </summary>
        public string LoadPath { get; set; }

        /// <summary>
        /// Gets or sets the path to save the game.
        /// </summary>
        public string SavePath { get; set; }

        /// <summary>
        /// Gets or sets the highscore's Name property.
        /// </summary>
        public string HighscoreSaveName { get; set; }

        /// <summary>
        /// Loads highscores.
        /// </summary>
        /// <returns>A collection of Highscore objects.</returns>
        public ICollection<Highscore> LoadHighscore()
        {
            return this.logic.LoadHighscore();
        }

        /// <summary>
        /// Renders the layout.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.BuildDrawing(drawingContext);
            }
        }

        private void LoadGame()
        {
            if (this.LoadPath == null)
            {
                // throw new ArgumentNullException("Path cannot be null");
                this.LoadPath = "GameLogic.Levels.LVL01.txt";
            }

            switch (this.Loadtype)
            {
                case StartGame.New:
                    this.logic.InitializeModel(this.LoadPath);
                    break;
                case StartGame.LoadSaved:
                    this.model = this.logic.LoadGame(this.LoadPath);
                    break;
                default: throw new NotImplementedException("Not a valid loading type");
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.sw = new Stopwatch();
            this.model = new Model(this.ActualWidth, this.ActualHeight);
            this.storageRepository = new StorageRepository(this.model);
            this.resource = new Resource();
            this.logic = new Logic(this.model, this.storageRepository, this.resource);

            // this.renderer.Reset();
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.LoadGame();
                win.KeyDown += this.Win_KeyDown;
            }
            else
            {
                this.model.Door = new Door(2, 2);
                this.model.Keys = new List<KeyToCollect>()
                {
                    new KeyToCollect(2, 1),
                };
                this.model.ThePlayer = new Player(1, 1, Direction.East);
                this.model.NumberOfKeysToOpenDoor = 1;
                this.model.Walls = new bool[4, 4];
            }

            this.renderer = new DungeonRenderer(this.model);

            this.logic.DoorOpenedEvent += (obj, args) => this.DoorOpened();
            this.logic.EnteringClosedDoorEvent += (obj, args) => this.TryEnterClosedDoor();
            this.logic.KeyCollectedEvent += (obj, args) => this.CollectKey();

            this.InvalidateVisual();
            this.sw.Start();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            bool finished = false;
            switch (e.Key)
            {
                case Key.Up:
                    finished = this.logic.Move();
                    break;
                case Key.Right:
                    this.logic.Rotate(1);
                    break;
                case Key.Left:
                    this.logic.Rotate(-1);
                    break;
                case Key.S:
                    this.sw.Stop();
                    this.logic.SaveGame(this.sw.Elapsed, this.SavePath);
                    MessageBox.Show($"Save was successful.\nElapsed time was: {(this.sw.Elapsed + this.model.ElapsedTime).ToString()}");
                    this.sw.Start();
                    break;
            }

            this.InvalidateVisual();
            this.logic.TryCollectKey();
            this.logic.TryOpenTheDoor();

            if (finished)
            {
                this.sw.Stop();
                TimeSpan completionTime = this.sw.Elapsed + this.model.ElapsedTime;
                MessageBox.Show($"You have reached the end of the dungeon in {completionTime.ToString(@"hh\:mm\:ss\.fff")}");
                if (this.HighscoreSaveName == null)
                {
                    this.HighscoreSaveName = DateTime.Now.ToString();
                }

                this.logic.SaveHighscore(completionTime, this.HighscoreSaveName);
                Window.GetWindow(this).Close();
            }
        }

        private void DoorOpened()
        {
            MessageBox.Show($"The Door has opened!");
        }

        private void TryEnterClosedDoor()
        {
            MessageBox.Show($"The Door is locked!");
        }

        private void CollectKey()
        {
            MessageBox.Show($"The Key on {this.model.ThePlayer.X}, {this.model.ThePlayer.Y} is collected.");
        }
    }
}
