﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

namespace GameRepository
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;
    using Newtonsoft.Json;

    /// <summary>
    /// StorageRepository to save games status and highscore.
    /// </summary>
    public class StorageRepository : IStorageRepository
    {
        private IGameModel model;
        private ICollection<Highscore> highscores;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageRepository"/> class.
        /// </summary>
        /// <param name="gameModel">Gamemodel interface, dependency injection.</param>
        public StorageRepository(IGameModel gameModel)
        {
            this.model = gameModel;
            this.highscores = new List<Highscore>();
        }

        /// <summary>
        /// Save the game status into a Json file.
        /// </summary>
        /// <param name="elapsedTime">Completion time which will be added to the gamemodel.</param>
        /// <param name="path">The actual game state saved here.</param>
        public void SaveGame(TimeSpan elapsedTime, string path)
        {
            this.model.ElapsedTime += elapsedTime;
            File.WriteAllText(path, JsonConvert.SerializeObject(this.model));
        }

        /// <summary>
        /// Load game status from Json file.
        /// </summary>
        /// <param name="path">Path of the Json file.</param>
        /// <returns>IGameModel interface with the loaded game's properties.</returns>
        public IGameModel LoadGame(string path)
        {
            // this.model = null;
            string stringmodel = File.ReadAllText(path);
            Model model = JsonConvert.DeserializeObject<Model>(stringmodel);
            this.model.Door = model.Door;
            this.model.NumberOfKeysToOpenDoor = model.NumberOfKeysToOpenDoor;
            this.model.ThePlayer = model.ThePlayer;
            this.model.Walls = model.Walls;
            this.model.ElapsedTime = model.ElapsedTime;
            return this.model;
        }

        /// <summary>
        /// Save highscore into a Json file.
        /// </summary>
        /// <param name="completionTime">The time of the completion of the map.</param>
        /// <param name="savename">Name of the save file.</param>
        public void SaveHighscore(TimeSpan completionTime, string savename)
        {
            if (File.Exists("./highscore.json"))
            {
                this.highscores = this.LoadHighscore();
                this.highscores.Add(new Highscore()
                {
                    ID = Guid.NewGuid().ToString(),
                    Name = savename,
                    ElapsedTime = completionTime,
                    Level = this.model.NumberOfKeysToOpenDoor.ToString(CultureInfo.CurrentCulture),
                });
                this.highscores = this.highscores.OrderBy(t => t.ElapsedTime.Duration()).ToList();
            }
            else
            {
                this.highscores.Add(new Highscore()
                {
                    ID = Guid.NewGuid().ToString(),
                    Name = savename,
                    ElapsedTime = completionTime,
                    Level = this.model.NumberOfKeysToOpenDoor.ToString(CultureInfo.CurrentCulture),
                });
            }

            string highscore = JsonConvert.SerializeObject(this.highscores);
            File.WriteAllText("./highscore.json", highscore);
        }

        /// <summary>
        /// Load the highscores from the Json file.
        /// </summary>
        /// <returns>Ilist of highscores.</returns>
        public ICollection<Highscore> LoadHighscore()
        {
            this.highscores = JsonConvert.DeserializeObject<IList<Highscore>>(File.ReadAllText("./highscore.json"));
            return this.highscores;
        }
    }
}
