﻿namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;
    using GameRepository;

    /// <summary>
    /// Specifies the loading possibilities.
    /// </summary>
    public enum StartGame
    {
        /// <summary>
        /// Loads a new game.
        /// </summary>
        New,

        /// <summary>
        /// Loads a saved game.
        /// </summary>
        LoadSaved,
    }

    /// <summary>
    /// Class that implements the games logic component.
    /// </summary>
    public class Logic : IGameLogic
    {
        private IGameModel model;
        private IStorageRepository storageRepo;
        private IResource resource;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="gameModel">Model instance that implements the IGameModel interface.</param>
        /// <param name="storageRepository">Repository instance that implements the IStorageRepository interface.</param>
        /// <param name="resource">Resource instance that implements the IResource interface.</param>
        public Logic(IGameModel gameModel, IStorageRepository storageRepository, IResource resource)
        {
            this.model = gameModel;
            this.storageRepo = storageRepository;
            this.resource = resource;
        }

        /// <summary>
        /// Event that is raised when the Door opens.
        /// </summary>
        public event EventHandler DoorOpenedEvent;

        /// <summary>
        /// Event that is raised when the player tries to enter the closed door.
        /// </summary>
        public event EventHandler EnteringClosedDoorEvent;

        /// <summary>
        /// Event that is raised when the player collects a key.
        /// </summary>
        public event EventHandler KeyCollectedEvent;

        /// <summary>
        /// Initialize the model if a new game is started.
        /// </summary>
        /// <param name="path">Path pointing to the file that contains the proper game model description.</param>
        public void InitializeModel(string path)
        {
            string[] gameSpace = this.resource.LoadLevel(path);
            if (gameSpace != null)
            {
                int x = int.MinValue;
                int y = int.MinValue;
                if (int.TryParse(gameSpace[0], out x) && int.TryParse(gameSpace[1], out y))
                {
                }
                else
                {
                    throw new ArgumentException("Game space does not contain the size of the map.");
                }

                this.model.Walls = new bool[x, y];

                Direction playerDirection = Direction.East;
                switch (gameSpace[2])
                {
                    case "N":
                        playerDirection = Direction.North;
                        break;
                    case "E":
                        playerDirection = Direction.East;
                        break;
                    case "S":
                        playerDirection = Direction.South;
                        break;
                    case "W":
                        playerDirection = Direction.West;
                        break;
                }

                for (int i = 0; i < this.model.Walls.GetLength(0); i++)
                {
                    for (int j = 0; j < this.model.Walls.GetLength(1); j++)
                    {
                        this.model.Walls[i, j] = gameSpace[j + 3][i] == 'F';
                        if (gameSpace[j + 3][i] == 'P')
                        {
                            this.model.ThePlayer = new Player(i, j, playerDirection);
                        }

                        if (gameSpace[j + 3][i] == 'D')
                        {
                            this.model.Door = new Door(i, j);
                        }

                        if (gameSpace[j + 3][i] == 'K')
                        {
                            this.model.Keys.Add(new KeyToCollect(i, j));
                        }
                    }
                }

                this.model.NumberOfKeysToOpenDoor = this.model.Keys.Count;
            }
            else
            {
                throw new ArgumentNullException(nameof(gameSpace), "Game space could not be loaded");
            }
        }

        /// <summary>
        /// Loads the game with the specified name.
        /// </summary>
        /// <param name="path">Path pointing to the file that contains the proper game model description.</param>
        /// <returns>An IGameModel object that contains the loaded data.</returns>
        public IGameModel LoadGame(string path)
        {
            this.model = this.storageRepo.LoadGame(path);
            return this.model;
        }

        /// <summary>
        /// Tries to move the player forward to the direction facing.
        /// </summary>
        /// <returns>Whether the player has reached the exit.</returns>
        public bool Move()
        {
            bool finished = false;
            int newX = this.model.ThePlayer.X;
            int newY = this.model.ThePlayer.Y;
            switch (this.model.ThePlayer.ActualDirection)
            {
                case Direction.North: newY = this.model.ThePlayer.Y - 1; break;
                case Direction.East: newX = this.model.ThePlayer.X + 1; break;
                case Direction.South: newY = this.model.ThePlayer.Y + 1; break;
                case Direction.West: newX = this.model.ThePlayer.X - 1; break;
            }

            if (newX >= 0 && newY >= 0 && newX < this.model.Walls.GetLength(0) && newY < this.model.Walls.GetLength(1) &&
                !this.model.Walls[newX, newY])
            {
                if (this.model.Door.X == newX && this.model.Door.Y == newY)
                {
                    if (!this.model.Door.Closed)
                    {
                        // actual move: the player reached the door that is open
                        this.model.ThePlayer.X = newX;
                        this.model.ThePlayer.Y = newY;
                        finished = true;
                    }
                    else
                    {
                        // the door is closed (lack of keys)
                        this.EnteringClosedDoorEvent?.Invoke(this, EventArgs.Empty);
                    }
                }
                else
                {
                    // actual move
                    this.model.ThePlayer.X = newX;
                    this.model.ThePlayer.Y = newY;
                }
            }

            return finished;
        }

        /// <summary>
        /// Changes the players direction.
        /// </summary>
        /// <param name="direction">If it is 1 then the player is rotated clockwise, else if it is -1 then counter clockwise.</param>
        public void Rotate(int direction)
        {
            if (direction == 1)
            {
                switch (this.model.ThePlayer.ActualDirection)
                {
                    case Direction.North:
                        this.model.ThePlayer.ActualDirection = Direction.East;
                        break;
                    case Direction.East:
                        this.model.ThePlayer.ActualDirection = Direction.South;
                        break;
                    case Direction.South:
                        this.model.ThePlayer.ActualDirection = Direction.West;
                        break;
                    case Direction.West:
                        this.model.ThePlayer.ActualDirection = Direction.North;
                        break;
                }
            }
            else if (direction == -1)
            {
                switch (this.model.ThePlayer.ActualDirection)
                {
                    case Direction.North:
                        this.model.ThePlayer.ActualDirection = Direction.West;
                        break;
                    case Direction.East:
                        this.model.ThePlayer.ActualDirection = Direction.North;
                        break;
                    case Direction.South:
                        this.model.ThePlayer.ActualDirection = Direction.East;
                        break;
                    case Direction.West:
                        this.model.ThePlayer.ActualDirection = Direction.South;
                        break;
                }
            }
        }

        /// <summary>
        /// Saves the actual state of the game.
        /// </summary>
        /// <param name="elapsedTime">The actual run time of the game.</param>
        /// <param name="path">The actual game state saved here.</param>
        public void SaveGame(TimeSpan elapsedTime, string path)
        {
            this.storageRepo.SaveGame(elapsedTime, path);
        }

        /// <summary>
        /// Method that collects a key when the player founds one.
        /// </summary>
        public void TryCollectKey()
        {
            foreach (KeyToCollect keyToCollect in this.model.Keys)
            {
                if (this.model.ThePlayer.Interact(keyToCollect))
                {
                    this.model.ThePlayer.Inventory++;
                    this.KeyCollectedEvent?.Invoke(this, EventArgs.Empty);
                }
            }

            this.model.Keys.Remove(this.model.Keys.SingleOrDefault(k => k.Interact(this.model.ThePlayer)));
        }

        /// <summary>
        /// Method that opens the door when the player has collected enough key(s).
        /// </summary>
        public void TryOpenTheDoor()
        {
            if (this.model.ThePlayer.Inventory.Equals(this.model.NumberOfKeysToOpenDoor) && this.model.Door.Closed)
            {
                this.model.Door.Closed = false;
                this.DoorOpenedEvent?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Saves the new highscore.
        /// </summary>
        /// <param name="completionTime">The completion time of the level.</param>
        /// <param name="savename">The players name for identifying their highscore.</param>
        public void SaveHighscore(TimeSpan completionTime, string savename)
        {
            this.storageRepo.SaveHighscore(completionTime, savename);
        }

        /// <summary>
        /// Method that gets the highscores.
        /// </summary>
        /// <returns>The collection of highscores.</returns>
        public ICollection<Highscore> LoadHighscore()
        {
            return this.storageRepo.LoadHighscore();
        }
    }
}
