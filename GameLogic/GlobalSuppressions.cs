﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "Do not need copyrigth")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Collection property must be able to set")]
[assembly: SuppressMessage("Microsoft.Design", "CA1014:Mark assemblies with CLSCompliant", Justification = "CSC file not found")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Multidimensional arrays needed.")]