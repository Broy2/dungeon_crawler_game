﻿namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the resources that the GameLogic will use.
    /// </summary>
    public interface IResource
    {
        /// <summary>
        /// Loads the saved model from a file and converts it into a string array.
        /// </summary>
        /// <param name="filename">The embedded resources route.</param>
        /// <returns>A string array filled with the loaded data.</returns>
        string[] LoadLevel(string filename);
    }
}
