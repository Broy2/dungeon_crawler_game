﻿namespace GameLogicTests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using GameLogic;
    using GameModel;
    using GameRepository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the GameLogics InitializeModel, Load and Save method.
    /// </summary>
    [TestFixture]
    internal class TestForModelFillingMethods
    {
        private Mock<IGameModel> mockedModel;
        private Mock<IStorageRepository> mockedStorageRepo;
        private Mock<IResource> mockedResource;
        private IGameModel expectedLoadedModel;

        /// <summary>
        /// Test for gamelogics save game method.
        /// </summary>
        [Test]
        public void TestSaveGame()
        {
            var logic = this.CreateGameLogicWithMocks();

            logic.SaveGame(It.IsAny<TimeSpan>(), It.IsAny<string>());

            this.mockedStorageRepo.Verify(srepo => srepo.SaveGame(It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Once);
            this.mockedStorageRepo.Verify(srepo => srepo.LoadGame(It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Test for gamelogics load game method.
        /// </summary>
        [Test]
        public void TestLoadGame()
        {
            var logic = this.CreateGameLogicWithMocks();

            var result = logic.LoadGame(It.IsAny<string>());

            Assert.That(result, Is.EqualTo(this.expectedLoadedModel));
            this.mockedStorageRepo.Verify(srepo => srepo.LoadGame(It.IsAny<string>()), Times.Once);
            this.mockedStorageRepo.Verify(srepo => srepo.SaveGame(It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Never);
        }

        /// <summary>
        /// Test for gamelogics initializemodel method.
        /// </summary>
        [Test]
        public void TestInitializeModel()
        {
            var logic = this.CreateGameLogicWithMocks();

            logic.InitializeModel(It.IsAny<string>());

            this.mockedResource.Verify(res => res.LoadLevel(It.IsAny<string>()), Times.Once);
        }

        private Logic CreateGameLogicWithMocks()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepo = new Mock<IStorageRepository>();

            IGameModel model = new Model(250, 650)
            {
                Door = new Door(2, 2),
                Keys = new List<KeyToCollect>()
                {
                    new KeyToCollect(2, 1),
                },
                ThePlayer = new Player(1, 1, Direction.East),
                NumberOfKeysToOpenDoor = 1,
                Walls = new bool[4, 4],
            };

            this.expectedLoadedModel = new Model(250, 650)
            {
                Door = new Door(2, 2),
                Keys = new List<KeyToCollect>()
                {
                    new KeyToCollect(2, 1),
                },
                ThePlayer = new Player(1, 1, Direction.East),
                NumberOfKeysToOpenDoor = 1,
                Walls = new bool[4, 4],
            };

            string[] gamespace = new string[7];
            gamespace[0] = "4";
            gamespace[1] = "4";
            gamespace[2] = "E";
            gamespace[3] = "FFFF";
            gamespace[4] = "FP F";
            gamespace[5] = "FKDF";
            gamespace[6] = "FFFF";

            this.mockedResource.Setup(res => res.LoadLevel(It.IsAny<string>())).Returns(gamespace);
            this.mockedStorageRepo.Setup(srepo => srepo.LoadGame(It.IsAny<string>())).Returns(model);
            this.mockedStorageRepo.Setup(srepo => srepo.SaveGame(It.IsAny<TimeSpan>(), It.IsAny<string>())).Verifiable();
            this.mockedModel.Setup(model => model.NumberOfKeysToOpenDoor).Returns(model.NumberOfKeysToOpenDoor);
            this.mockedModel.Setup(model => model.Keys.Count).Returns(model.Keys.Count);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepo.Object, this.mockedResource.Object);
        }
    }
}
