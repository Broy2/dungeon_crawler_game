﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Gamemodel class describes the objects of the game.
    /// </summary>
    public class Model : IGameModel
    {
        /// <summary>
        /// Sightdistance of the player.
        /// </summary>
        public const int SightDistance = 2;

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class.
        /// </summary>
        /// <param name="w">The width of the container window.</param>
        /// <param name="h">The height of the container window.</param>
        public Model(double w, double h)
        {
            this.GameWidth = w;
            this.GameHeight = h;
            this.Keys = new List<KeyToCollect>();
            this.NumberOfKeysToOpenDoor = 1;
        }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        public Player ThePlayer { get; set; }

        /// <summary>
        /// Gets or sets the door leading to the exit.
        /// </summary>
        public Door Door { get; set; }

        /// <summary>
        /// Gets or sets the number of keys on the level.
        /// </summary>
        public ICollection<KeyToCollect> Keys { get; set; }

        /// <summary>
        /// Gets or sets the position of the walls on the map presented by bool values.
        /// </summary>
        public bool[,] Walls { get; set; }

        /// <summary>
        /// Gets or sets the number of keys required to open the door.
        /// </summary>
        public int NumberOfKeysToOpenDoor { get; set; }

        /// <summary>
        /// Gets or sets the sum of the running time.
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }

        /// <summary>
        /// Gets the Width of the game window.
        /// </summary>
        public double GameWidth { get; private set; }

        /// <summary>
        /// Gets the height of the game window.
        /// </summary>
        public double GameHeight { get; private set; }

        /// <summary>
        /// Proper unit testing needed overwritten ToString() method.
        /// </summary>
        /// <returns>The info about the coordinates of the player and exit, plus the level in one string.</returns>
        public override string ToString()
        {
            return $"Player({this.ThePlayer.X},{this.ThePlayer.Y})\nExit({this.Door.X},{this.Door.Y})\nLevel:{this.NumberOfKeysToOpenDoor}";
        }

        /// <summary>
        /// Proper unit testing needed overwritten Equals(object obj) method.
        /// </summary>
        /// <param name="obj">An object that is compared to the GameModel.</param>
        /// <returns>The boolean value whether the parameter is equal with the GameModel.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Model))
            {
                return false;
            }

            var param = obj as Model;
            return param.Door.Equals(this.Door) && param.ThePlayer.Equals(this.ThePlayer) && param.GameHeight.Equals(this.GameHeight) && param.GameWidth.Equals(this.GameWidth) && param.Keys.SequenceEqual(this.Keys) && param.NumberOfKeysToOpenDoor.Equals(this.NumberOfKeysToOpenDoor) && param.Walls.LongLength.Equals(this.Walls.LongLength);
        }

        /// <summary>
        /// Proper unit testing needed overridden GetHashCode() method.
        /// </summary>
        /// <returns>A possible identifier of the Model.</returns>
        public override int GetHashCode()
        {
            return (int)((9761 * this.GameHeight * this.GameWidth / this.NumberOfKeysToOpenDoor) % 27);
        }
    }
}
