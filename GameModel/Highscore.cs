﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class for highscore model.
    /// </summary>
    public class Highscore
    {
        /// <summary>
        /// Gets or sets the identifier of a highscore object.
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets the name of a highscore object.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number of the game level.
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Gets or sets the completion time of the level.
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }

        /// <summary>
        /// Proper unit testing needed overwritten Equals(object obj) method.
        /// </summary>
        /// <param name="obj">An object that is compared to the Player.</param>
        /// <returns>The boolean value whether the parameter is equal with the Player object.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Highscore))
            {
                return false;
            }

            var param = obj as Highscore;
            return param.ID == this.ID;
        }

        /// <summary>
        /// Proper unit testing needed overridden GetHashCode() method.
        /// </summary>
        /// <returns>The unique identifier's HashCode() of the Highscore object.</returns>
        public override int GetHashCode()
        {
            return this.ID.GetHashCode(StringComparison.Ordinal);
        }

        /// <summary>
        /// Overriding the tostring method.
        /// </summary>
        /// <returns>A nice highscore format.</returns>
        public override string ToString()
        {
            return $"{this.Name} - {this.Level} - {this.ElapsedTime.ToString()}";
        }
    }
}
