﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The Door game item class.
    /// </summary>
    public class Door : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// Constructor of the door class.
        /// </summary>
        /// <param name="coordX">X coordinate of the Door.</param>
        /// <param name="coordY">Y coordinate of the Door.</param>
        public Door(int coordX, int coordY)
        {
            this.X = coordX;
            this.Y = coordY;
            this.Closed = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the door is closed or opened.
        /// </summary>
        public bool Closed { get; set; }

        /// <summary>
        /// Proper unit testing needed overwritten Equals(object obj) method.
        /// </summary>
        /// <param name="obj">An object that is compared to the Door.</param>
        /// <returns>The boolean value whether the parameter is equal with the Door object.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Door))
            {
                return false;
            }

            var param = obj as Door;
            return param.X == this.X && param.Y == this.Y && param.Closed == this.Closed;
        }

        /// <summary>
        /// Proper unit testing needed overridden GetHashCode() method.
        /// </summary>
        /// <returns>The default hash.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
