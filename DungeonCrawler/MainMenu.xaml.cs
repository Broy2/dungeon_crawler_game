﻿namespace DungeonCrawler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainMenu.xaml.
    /// </summary>
    public partial class MainMenu : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            this.InitializeComponent();
        }

        private void NewGameClick(object sender, RoutedEventArgs e)
        {
            DungeonWindow win = new DungeonWindow();
            GameControl.Control ctrl = win.Content as GameControl.Control;
            ctrl.Loadtype = GameLogic.StartGame.New;
            ctrl.LoadPath = $"GameLogic.Levels.{((ComboBoxItem)this.cbBox.SelectedItem).Content.ToString()}.txt";
            ctrl.SavePath = "testsave.json";
            win.Show();
        }

        private void LoadGameClick(object sender, RoutedEventArgs e)
        {
            DungeonWindow win = new DungeonWindow();
            GameControl.Control ctrl = win.Content as GameControl.Control;
            ctrl.Loadtype = GameLogic.StartGame.LoadSaved;
            ctrl.LoadPath = "testsave.json";
            ctrl.SavePath = "testsave.json";
            win.Show();
        }

        private void HighscoreClick(object sender, RoutedEventArgs e)
        {
            HighscoreWindow hwin = new HighscoreWindow();
            hwin.Show();
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
