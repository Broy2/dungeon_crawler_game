﻿namespace DungeonCrawler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for HighscoreWindow.xaml.
    /// </summary>
    public partial class HighscoreWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreWindow"/> class.
        /// </summary>
        public HighscoreWindow()
        {
            this.InitializeComponent();
            DungeonWindow win = new DungeonWindow();
            GameControl.Control ctrl = win.Content as GameControl.Control;

            win.Show();
            win.Close();
            this.lbox.ItemsSource = ctrl.LoadHighscore();
        }
    }
}
